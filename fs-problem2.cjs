/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs');
const path = require('path');
const initialReadPath = path.join(__dirname, './lipsum (1).txt');

function main() {
    function readFile(toReadPath) {
        return new Promise((resolve, reject) => {
            fs.readFile( toReadPath, 'utf-8', function (error, data) {
                if(error){
                    console.log('error while reading file:');
                    reject(err);
                } else {
                    console.log(`data has been read from ${toReadPath}`);
                    resolve(data);
                }
            });
        })
    }

    function writeData (writeTo, data) {
        return new Promise((resolve, reject) => {
            fs.writeFile(writeTo, data, (err) => {
                if(err) {
                    console.log('error while writing data:');
                    reject(err);
                } else {
                    console.log('data successfully written');
                    const writtenTo = writeTo;
                    resolve(writtenTo);
                }
            });
        });
    }

    function appendFile (appendTo, fileName) {
        return new Promise((resolve, reject) => {
            fs.appendFile(appendTo, fileName, (err) => {
                if(err) {
                    console.log('error while appending file name:');
                    reject(err);
                } else {
                    console.log(`${fileName} successfully appended to ${appendTo}`);
                    resolve(fileName);
                }
            })
        })
    }

    function deleteFiles (arrOfFiles) {
        return new Promise((resolve, reject) => {
            let deleteCount = 0;
            for(let file of arrOfFiles) {
                fs.unlink(file, (err) => {
                    if(err) {
                        console.log('error while deleting:');
                        reject(err);
                    } else {
                        deleteCount += 1;
                        console.log(`successfully deleted ${file}`);
                        if(deleteCount === 3) {
                            resolve('all files have been deleted!');
                        }
                    }
                })
            }
        })
    }

    // 1)
    readFile(initialReadPath)
        .then((data) => {
            // 2)
            const upperCaseData = data.toUpperCase();
            return writeData('./uppercase.txt', upperCaseData);
        })
        .then((writtenTo) => {
            return appendFile('./filenames.txt', `${writtenTo}\n`);
        })
        .then((lastWrittenTo) => {
            // the last written string contains a 'new line character'
            return readFile(lastWrittenTo.replace('\n', ''));
        })
        .then((data) => {
            // 3) 
            const splitContent = data.toLowerCase().split('. ');
            return writeData('./split.txt', JSON.stringify(splitContent));
        })
        .then((writtenTo) => {
            return appendFile('./filenames.txt', `${writtenTo}\n`);
        })
        .then((lastWrittenTo) => {
            // the last written string contains a 'new line character'
            return readFile(lastWrittenTo.replace('\n', ''));
        })
        .then((splitData) => {
            // 4) 
            const sorted = JSON.parse(splitData).sort();
            return writeData('./sorted.txt', JSON.stringify(sorted));
        })
        .then((writtenTo) => {
            return appendFile('./filenames.txt', `${writtenTo}`);
        })
        .then((lastWrittenTo) => {
            // 5)
            return readFile('./filenames.txt');
        })
        .then((fileNames) => {
            const arrOfFiles = fileNames.split('\n');
            return deleteFiles(arrOfFiles);
        })
        .then((result) => {
            console.log('final statement:', result);
        })
        .catch((error) => {
            console.log(error);
        })
}

module.exports = main;