/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const fs = require('fs');
const path = require('path');

function main(absoluteRandomDirPath, numberOfFiles) {
    
    function deleteRandomFiles(absoluteRandomDirPath, fileNamesArray) { 
        return new Promise((resolve, reject) => {        
            for(let index = 0; index < fileNamesArray.length; index ++) {
                const pathToDelete = path.join(absoluteRandomDirPath, fileNamesArray[index]);
                fs.unlink(pathToDelete, function (error) {
                    if(error) {
                        console.log('error while deleting');
                        reject(error);
                    } else {
                        console.log(`file ${fileNamesArray[index]} successfully deleted`);
                        if(index === fileNamesArray.length -1) {
                            resolve(`all files have been successfully deleted`);
                        }
                    }
                });
            } 
        })
    }
    
    function createRandomJsonFiles (absoluteRandomDirPath, numberOfFiles) {
        return new Promise((resolve, reject) => {

            let completedExecutions = 0;
            const fileNamesArray = [];
            for(let num = 1 ; num <= numberOfFiles; num ++) {
        
                const fileName = `file${num}.json`;
                const filePath = path.join(absoluteRandomDirPath, fileName);
                const fileData = {
                    id: num + Math.random() ,
                    name: `File ${num}`
                };
        
                fs.writeFile(filePath, JSON.stringify(fileData), (err) => {
                    if(err) {
                        reject(err);
                    } else {
                        completedExecutions += 1;
                        console.log(`file ${fileName} created.`);
                        fileNamesArray.push(fileName);
        
                        if(completedExecutions == numberOfFiles) {
                            // proceed with deletion
                            resolve([absoluteRandomDirPath, fileNamesArray]);
                        }
                    }
                });
            }
        })
    };
    
    function createADirectory(absoluteRandomDirPath, numberOfFiles) {
        return new Promise((resolve, reject) => {
            fs.mkdir(absoluteRandomDirPath, function (error) {
                if(error) {
                    reject(error);
                } else {
                    resolve([absoluteRandomDirPath, numberOfFiles])
                    // createRandomJsonFiles(absoluteRandomDirPath, numberOfFiles);
                }
            });
    
        });
    }

    createADirectory(absoluteRandomDirPath, numberOfFiles)
        .then(([absoluteRandomDirPath, numberOfFiles]) => {
            return createRandomJsonFiles(absoluteRandomDirPath, numberOfFiles);
        })
        .then(([absoluteRandomDirPath, fileNamesArray]) => {
            return deleteRandomFiles(absoluteRandomDirPath, fileNamesArray)
        })
        .then((result) => {
            console.log(result);
        })
        .catch((err) => {
            console.log(err);
        })
}

module.exports = main;