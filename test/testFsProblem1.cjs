const fsPromblem1 = require('../fs-problem1.cjs');

const path = require('path');

const absoluteRandomDirPath = path.resolve('./random_json');
const numberOfFiles = 5;

fsPromblem1(absoluteRandomDirPath, numberOfFiles);